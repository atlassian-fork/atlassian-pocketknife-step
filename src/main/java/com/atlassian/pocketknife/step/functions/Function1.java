package com.atlassian.pocketknife.step.functions;

@FunctionalInterface
public interface Function1<T, R> {

    R apply(T t);

}
