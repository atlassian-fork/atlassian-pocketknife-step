package com.atlassian.pocketknife.step;

import com.atlassian.pocketknife.step.functions.Function5;
import com.atlassian.pocketknife.step.ops.EitherStep;
import io.atlassian.fugue.Either;

public class EitherStep5<E1, E2, E3, E4, E5, E> extends EitherStep {
    private final Either<E, E1> either1;
    private final Either<E, E2> either2;
    private final Either<E, E3> either3;
    private final Either<E, E4> either4;
    private final Either<E, E5> either5;

    EitherStep5(Either<E, E1> either1,
                Either<E, E2> either2,
                Either<E, E3> either3,
                Either<E, E4> either4,
                Either<E, E5> either5) {
        this.either1 = either1;
        this.either2 = either2;
        this.either3 = either3;
        this.either4 = either4;
        this.either5 = either5;
    }

    public <Z> Either<E, Z> yield(Function5<E1, E2, E3, E4, E5, Z> function5) {
        return either1.flatMap(value1 ->
                either2.flatMap(value2 ->
                        either3.flatMap(value3 ->
                                either4.flatMap(value4 ->
                                        either5.map(value5 -> function5.apply(value1, value2, value3, value4, value5))))));
    }

}