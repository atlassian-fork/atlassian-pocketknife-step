package com.atlassian.pocketknife.step;

import io.atlassian.fugue.Option;
import org.junit.Assert;
import org.junit.Test;

import static com.atlassian.pocketknife.step.NewerSupport.firstNGOption;
import static com.atlassian.pocketknife.step.NewerSupport.firstOKOption;
import static com.atlassian.pocketknife.step.NewerSupport.secondNGOption;
import static com.atlassian.pocketknife.step.NewerSupport.secondOKOption;
import static com.atlassian.pocketknife.step.NewerSupport.thirdOKOption;

public class TestOptionSteps {

    private static final String STRING = "123456";
    private static final String STRING_UPPERED = "QWERTY";
    private static final String STRING_LOWERED = "qwerty";
    private static final Long LONG = 123456L;
    private static final Long LONGLONG = 123456123456L;

    @Test
    public void test_1_step_success() {
        Option<Long> stepped = Steps
                .begin(firstOKOption(STRING))
                .yield(value1 -> new Long(value1));

        Assert.assertTrue("Should be success", stepped.isDefined());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONG));
    }

    @Test
    public void test_2_step_success() {
        Option<Long> stepped = Steps
                .begin(firstOKOption(STRING))
                .then((firstValue) -> secondOKOption(STRING))
                .yield((value1, value2) -> new Long(value1 + value2));

        Assert.assertTrue("Should be success", stepped.isDefined());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONGLONG));
    }

    @Test
    public void test_3_step_success() {
        Option<Long> stepped = Steps
                .begin(firstOKOption(STRING))
                .then(() -> secondOKOption(STRING))
                .then(() -> firstOKOption(STRING))
                .yield((value1, value2, value3) -> new Long(value1 + value2));

        Assert.assertTrue("Should be success", stepped.isDefined());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONGLONG));
    }

    @Test
    public void test_4_step_success() {
        Option<Long> stepped = Steps
                .begin(firstOKOption(STRING))
                .then(() -> secondOKOption(STRING))
                .then((first, second) -> Option.some(first + second))
                .then(() -> thirdOKOption(STRING))
                .yield((value1, value2, value3, value4) -> new Long(value3));

        Assert.assertTrue("Should be success", stepped.isDefined());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONGLONG));
    }

    @Test
    public void test_5_step_success() {
        Option<Long> stepped = Steps
                .begin(firstOKOption(STRING))
                .then(() -> secondOKOption(STRING))
                .then((first, second) -> Option.some(first + second))
                .then(() -> thirdOKOption(STRING))
                .then(() -> thirdOKOption(STRING))
                .yield((value1, value2, value3, value4, value5) -> new Long(value3));

        Assert.assertTrue("Should be success", stepped.isDefined());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONGLONG));
    }

    @Test
    public void test_1_step_failure() {
        Option<Long> stepped = Steps
                .begin(firstNGOption(STRING))
                .yield(value1 -> new Long(value1));

        Assert.assertTrue("Should be failure", stepped.isEmpty());
    }

    @Test
    public void test_2_step_failure() {
        Option<Long> stepped = Steps
                .begin(firstNGOption(STRING))
                .then(() -> secondOKOption(STRING))
                .yield((value1, value2) -> new Long(value1));

        Assert.assertTrue("Should be failure", stepped.isEmpty());
    }

    @Test
    public void test_3_step_failure() {
        Option<Long> stepped = Steps
                .begin(firstOKOption(STRING))
                .then(() -> secondNGOption(STRING))
                .then(() -> secondOKOption(STRING))
                .yield((value1, value2, value3) -> new Long(value1));

        Assert.assertTrue("Should be failure", stepped.isEmpty());
    }

    @Test
    public void test_4_step_failure() {
        Option<Long> stepped = Steps
                .begin(firstOKOption(STRING))
                .then(() -> secondOKOption(STRING))
                .then((s, s2) -> secondOKOption(STRING))
                .then(() -> secondNGOption(STRING))
                .yield((value1, value2, value3, value4) -> new Long(value1));

        Assert.assertTrue("Should be failure", stepped.isEmpty());
    }

    @Test
    public void test_5_step_failure() {
        Option<Long> stepped = Steps
                .begin(firstOKOption(STRING))
                .then(s -> secondOKOption(STRING))
                .then((s, s2) -> secondOKOption(STRING))
                .then(() -> secondOKOption(STRING))
                .then((value1, value2, value3, value4) -> secondNGOption(STRING))
                .yield((value1, value2, value3, value4, value5) -> new Long(value1));

        Assert.assertTrue("Should be failure", stepped.isEmpty());
    }


}
